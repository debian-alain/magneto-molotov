#!/bin/bash

OUTPUT=~/SCREENSHOT

[ -d "$OUTPUT" ] || mkdir "$OUTPUT"


WIDHT=$(xwininfo -name "Molotov" | awk '/Width:/ { print $2 }')
HEIGHT=$(xwininfo -name "Molotov" | awk '/Height:/ { print $2 }')

ABSOLUTE_X=$(xwininfo -name "Molotov" | awk '/Absolute upper-left X:/ { print $4 }')
ABSOLUTE_Y=$(xwininfo -name "Molotov" | awk '/Absolute upper-left Y:/ { print $4 }')


ffmpeg -thread_queue_size 512 -f pulse -ac 2 -i $(pactl list sources | sed -n '/alsa.*monitor/s/.*: \(.*\)$/\1/p') -f x11grab -s "$WIDHT"x"$HEIGHT" -i :0.0+"$ABSOLUTE_X","$ABSOLUTE_Y" -r 30 -acodec libmp3lame -vcodec libx264 -preset ultrafast -pix_fmt yuv444p -threads 0 -loglevel repeat+verbose "$OUTPUT/[Le $JOUR à $HEURE]_SCREENSHOT.mkv"


